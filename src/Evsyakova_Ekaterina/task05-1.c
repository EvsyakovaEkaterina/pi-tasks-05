#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 256
FILE *fp;
struct BOOK {
	char title[N];
	char author[N];
	int year;
};
int main() {
	fp = fopen("D:\\input.txt", "r");
	struct BOOK* book = (struct BOOK*)malloc(sizeof(struct BOOK));
	char s[N];
	int n = 0;
	int i,j;
	while (fgets(s, N, fp)) 
	{
		++n;
		book = (struct BOOK*)realloc(book, n * sizeof(struct BOOK));
		book[n - 1].year = 0;
		i = 0;
		j = 0;
		while (s[i] != '\t') book[n - 1].title[j++] = s[i++];
		book[n - 1].name[j] = '\0';
		++i;
		j = 0;
		while (s[i]!='\t') book[n - 1].author[j++] = s[i++];
		book[n - 1].author[j] = '\0';
		++i;
		book[n - 1].year = 0;
		while (s[i] != '\n')
			book[n - 1].year = book[n - 1].year * 10 + s[i++] - '0';

	}
	for (i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].title, book[i].author, book[i].year);
	int newBook = 0, oldBook = 0;
	for (i = 0; i < n; ++i) 
	{
		if (book[i].year > book[newBook].year)
			newBook = i;
		if (book[i].year < book[oldBook].year)
			oldBook = i;
	}
	printf("Oldest book: %s\t%s\t%i\n", book[oldBook].title, book[oldBook].author, book[oldBook].year);
	printf("Newest book: %s\t%s\t%i\n", book[newBook].title, book[newBook].author, book[newBook].year);
	for (i = 0; i < n; ++i) {
		int imin = i;
		for (j = i + 1; j < n; ++j) {
			if (strcmp(book[j].author, book[imin].author) < 0) imin = j;
		}
		struct BOOK k = book[i];
		book[i] = book[imin];
		book[imin] = k;
	}
	for (i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].title, book[i].author, book[i].year);
	return 0;
	free(book);
}